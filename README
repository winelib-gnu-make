====================================================
GNU make based build system for Winelib applications
====================================================

:Author:        Kirill Smelkov <kirr@mns.spb.ru>
:Date:          2007 September 2nd
:License:       LGPL


I am going to port some Win32 applications to GNU/Linux and thought it would be
useful to improve the build system for Winelib applications.


Current status and Motivation
=============================

1. Wine in-tree Makefiles for programs & dlls are pretty much good and
   maintainable, but they lack C++ support.
2. Initial approach taken by winemaker (I used it in 2003-2004) was to adapt
   Wine's make rules to support C++ .
3. At present winemaker dropped supplying above mentioned rules and generates
   very simple makefiles. To me this approach have several problems:

- lack of dependency tracking
- hard to maintain Makefiles.
  The rules generated are substituted in each Makefile instead of being in one
  place.
- good as a starting point, but not a complete solution.


Winemaker already relies on GNU toolchain (in form of gcc and g++), and I
though::

  Why not use GNU make with all available extensions when necessary, and deploy
  a simple full-featured build system for Winelib based applications

The result is presented here for public review.
Comments, suggestions?

Features
========

* support C, C++ and resource files.
* automatic dependency tracking for C and C++.
* beautified build output (to increase SNR and concentrate on errors and
  warnings).
* winemaker patches provided (although I don't know Perl -- help needed, Francois?).
* Tested with GNU make 3.81. This version of GNU make was released in 2006 and
  is present in all recent Linux distributions.

Usage example
=============

For usage example see directory clock+wow32/ . There I provide example of how
to build 1 program and 2 dlls.

Original clock.exe & wow32.dll processed by winemaker are located in misc/ for comparison.
