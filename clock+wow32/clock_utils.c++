#include <stdio.h>
#include <vector>
using namespace std;

extern "C"
void clock_utils_test()
{
    vector<int> v;
    int i;

    for (i=0;i<10;++i)
        v.push_back(i);

    for (vector<int>::iterator it=v.begin(); it!=v.end(); ++it)
        fprintf(stderr, "%i\n", *it);

}
